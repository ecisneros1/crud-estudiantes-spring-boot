package com.app.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.app.web.entity.Estudiante;
import com.app.web.repository.EstudianteRepository;

@SpringBootApplication
public class CrudSpringApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CrudSpringApplication.class, args);
	}
	
	@Autowired
	private EstudianteRepository repositorio;

	@Override
	public void run(String... args) throws Exception {
		/*
		Estudiante estudiante1 = new Estudiante("Emanuel",
				"Cisneros",
				"emanuel.a.cisneros@gmail.com");
		repositorio.save(estudiante1);
		
		Estudiante estudiante2 = new Estudiante("Franco",
				"Simonazzi",
				"franco.simonazzi@gmail.com");
		repositorio.save(estudiante2);
		*/
	}

}
